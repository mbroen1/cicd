#!/usr/bin/python3
import os
import requests
import getopt
import sys

CI_VERSION_VAR='LATEST_VERSION' if not 'CI_VERSION_VAR' in os.environ else os.environ['CI_VERSION_VAR']

class Version():
    def __init__(self):
        self._is_new, version = (False, os.environ[CI_VERSION_VAR]) if CI_VERSION_VAR in os.environ else (True, '0.0.0')
        parts = version.split('.')
        self._major = int(parts[0])
        self._minor = int(parts[1])
        self._patch = int(parts[2])

    def major(self):
        self._major += 1
        self._minor = 0
        self._patch = 0

    def minor(self):
        self._minor += 1
        self._patch = 0

    def patch(self):
        self._patch += 1

    def __str__(self):
        return '.'.join([str(self._major), str(self._minor), str(self._patch)])

    def _url(self):
        url = '/'.join([
            os.environ['CI_API_V4_URL'],
            'projects',
            os.environ['CI_PROJECT_ID'],
            'variables',
        ])
        return url if self._is_new else '/'.join([url, CI_VERSION_VAR])

    def submit(self):
        data = { 'key': CI_VERSION_VAR, 'value': str(self) } if self._is_new else { 'value': str(self) }
        method = 'POST' if self._is_new else 'PUT'
        headers ={ 'JOB-TOKEN': os.environ['CI_JOB_TOKEN'] }
        return requests.request(method, self._url(), headers=headers, data=data)

def usage():
    print("%s ([-M | --major] | [-m | --minor] | [-p | --patch] | [-h | --help])" % (sys.argv[0]) )

def main(argv):
    version = Version()
    try:
        opts, args = getopt.getopt(argv, 'Mmph', ['major', 'minor', 'patch', 'help'])
    except getopt.GetoptError:
        usage()
        exit(1)

    if len(opts) == 0:
        print(version)
        exit(0)

    elif len(opts) == 1:
        opt = opts[0][0]
        if opt in ('-M', '--major'):
            version.major()
        elif opt in ('-m', '--minor'):
            version.minor()
        elif opt in ('-p', '--patch'):
            version.patch()
        elif opt in ('-h', '--help'):
            usage()
            exit(0)
    else:
        usage()
        exit(1)

    version.submit()
    print(version)

if __name__ == '__main__':
    main(sys.argv[1:])
