# CI/CD Helper tools
A collection of tools to assist in creating CI/CD pipelines.

## Version
Stores the version of a project using [GitLab's project level varaible API](https://docs.gitlab.com/ee/api/project_level_variables.html). Versioning follows the [Semantic Versioning](https://semver.org/) specification.
```bash
# Examples
./version         # Print current version
#> 1.5.42
./version -p      # Increment patch version
#> 1.5.43
./version -m      # Increment minor version
#> 1.6.0
./version --patch # Increment patch version again
#> 1.6.1
./version -M      # Increment major version
#> 2.0.0
```
## Publish
Publish a file or directory using [GitLab's generic packages repository](https://docs.gitlab.com/ee/user/packages/generic_packages/). Directories are flattened and their names used as a filename prefix.
```bash
# Examples
tree --dirsfirst build
#> build
#> ├── arm
#> │   └── package.tar.gz
#> ├── x86
#> │   └── package.tar.gz
#> ├── x86_64
#> │   └── package.tar.gz
#> └── README.md
#>
#> 3 directories, 4 files
./publish -i build # Publish build directory recusively
#> {'file': 'build/x86/package.tar.gz', 'url': '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${LATEST_VERSION}/x86-package.tar.gz'}
#> {'file': 'build/x86_64/package.tar.gz', 'url': '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${LATEST_VERSION}/x86_64-package.tar.gz'}
#> {'file': 'build/arm/package.tar.gz', 'url': '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${LATEST_VERSION}/arm-package.tar.gz'}
#> {'file': 'build/README.md', 'url': '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${LATEST_VERSION}/README.md'}
./publish -i build/README.md --simulate # Simulate publishing a single file
#> {'file': 'build/README.md', 'url': '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${LATEST_VERSION}/build-README.md'}
```

## Templates
A collection of templates to be used in Gitlab's CI pipeline.

### Docker Build
Build, tag, and publish docker images. By default images are tagged as `latest` and published to the [Gitlab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/).
```yaml
# Include template
include:
 - project: 'mbroen1/cicd'
   ref: master
   file: '/templates/docker-build.yml'

# Create a docker build job
builder:
  extends: .docker-build
  variables:
    DOCKERFILE: Dockerfile
  only:
    changes:
      - Dockerfile
```
